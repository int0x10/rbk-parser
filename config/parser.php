<?php

return [
    'path_to_saved_images' => dirname(__DIR__) . '/public/images',
    'limit' => 15,
    'rbc' => [
        'url' => 'https://rbc.ru',
        'news_container_class' => '.js-news-feed-list',
        'news_item_class' => '.news-feed__item',
        'news_content_class' => '.article__text p',
        'image_item_class' => '.article__main-image__image',
    ],
];