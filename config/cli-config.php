<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Int0x10\RbkParser\AppContainer;

require __DIR__ . '/../vendor/autoload.php';

return ConsoleRunner::createHelperSet(
    (new AppContainer())->get('doctrine')
);
