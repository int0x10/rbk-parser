<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

$config = Setup::createAnnotationMetadataConfiguration([
    dirname(__DIR__) . '/src/Entity'
], true, null, null, false);

return EntityManager::create([
    'driver' => 'pdo_sqlite',
    'path' => dirname(__DIR__) . '/var/db.sqlite',
], $config);