<?php

namespace Int0x10\RbkParserCli;

use Doctrine\ORM\EntityManagerInterface;
use Int0x10\RbkParser\AppContainer;
use Int0x10\RbkParser\Entity\Article;
use Int0x10\RbkParser\ParserBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\SingleCommandApplication;

require dirname(__DIR__) . '/vendor/autoload.php';

(new SingleCommandApplication())
    ->addArgument('type', InputArgument::REQUIRED, 'Аргумент типа парсера для сайта')
    ->setCode(function (InputInterface $input, OutputInterface $output): void {
        /** @var EntityManagerInterface $em */
        $em = (new AppContainer())->get('doctrine');
        $parser = (new ParserBuilder())->get($input->getFirstArgument());
        $parser->rewind();

        while ($parser->valid()) {
            /** @var array $data */
            $data = $parser->current();
            $output->writeln('Saving: ' . $data['title']);

            $article = (new Article())
                ->setTitle($data['title'])
                ->setContent($data['content'])
                ->setImage($data['imageFileName']);

            $em->persist($article);
            $parser->next();
        }

        $em->flush();
    })
    ->run();