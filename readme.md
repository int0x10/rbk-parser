# Тестовое задание

Спарсить первые 15 новостей с rbс.ru 

![image](Screenshot.png "screenshot")

### Используются:

* Slim (https://www.slimframework.com/)
* Goutte (https://github.com/FriendsOfPHP/Goutte)
* Doctrine ORM (https://www.doctrine-project.org/projects/orm.html)
* SQLite

### Setup
```bash
composer install
composer schema-create
composer parse
composer serve
```