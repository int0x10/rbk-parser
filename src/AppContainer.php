<?php

namespace Int0x10\RbkParser;

use DI\Container;
use Psr\Container\ContainerInterface;

class AppContainer implements ContainerInterface
{
    private Container $container;

    public function __construct()
    {
        $this->container = new Container();
        $this->build();
    }

    private function build(): void
    {
        $this->container->set('doctrine', function () {
            return require dirname(__DIR__) . '/config/doctrine.php';
        });
    }

    public function get(string $id): object
    {
        return $this->container->get($id);
    }

    public function has(string $id): bool
    {
        return $this->container->has($id);
    }
}
