<?php

namespace Int0x10\RbkParser;

use Exception;
use Goutte\Client;
use Iterator;
use SplQueue;
use Symfony\Component\DomCrawler\Crawler;

class Parser
{
    private Client $client;
    private string $url;
    private int $limit;
    private string $pathToSavedImages;
    private string $newsContainerClass;
    private string $newsItemClass;
    private string $newsContentClass;
    private ?string $articleImageClass = null;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return array
     */
    private function getNewsLinks(): array
    {
        $crawler = $this->client->request('GET', $this->url);

        $previewBlock = $crawler
            ->filter($this->newsContainerClass)
            ->first();

        $links = $previewBlock
            ->filter($this->newsItemClass)
            ->each(function (Crawler $node): string {
                return $node->link()->getUri();
            });

        return array_slice($links, 0, $this->limit);
    }

    /**
     * @param string $url
     * @return string
     */
    private function getFileNameByUrl(string $url): string
    {
        $exp = explode('/', $url);

        return end($exp);
    }

    /**
     * @param string $url
     * @param string $fileName
     * @return void
     */
    private function downloadImage(string $url, string $fileName): void
    {
        file_put_contents(
            $this->pathToSavedImages . '/' . $fileName,
            file_get_contents($url)
        );
    }

    /**
     * @param Crawler $crawler
     * @return string|null
     */
    private function getTitle(Crawler $crawler): ?string
    {
        try {
            return $crawler
                ->filter('h1')
                ->first()
                ->text();
        } catch (Exception $exception) {
            return null;
        }
    }

    /**
     * @param Crawler $crawler
     * @return string|null
     */
    private function getContent(Crawler $crawler): ?string
    {
        try {
            return join(
                "\n",
                $crawler
                    ->filter($this->newsContentClass) // .article__text p
                    ->each(function (Crawler $node): string {
                        return $node->text();
                    })
            );
        } catch (Exception $exception) {
            return null;
        }
    }

    /**
     * @param Crawler $crawler
     * @return string|null
     */
    private function getImageUri(Crawler $crawler): ?string
    {
        try {
            return $crawler
                ->filter($this->articleImageClass)
                ->image()
                ->getUri();
        } catch (Exception $exception) {
            return null;
        }
    }

    /**
     * @param string $url
     * @return array
     * @throws Exception
     */
    private function getArticleByUrl(string $url): array
    {
        $article = $this->client->request('GET', $url);
        $title = $this->getTitle($article);
        $content = $this->getContent($article);
        $imageFileName = null;

        if ($title === null || $content === null) {
            throw new Exception('Url ' . $url . ' не содержит обязательных элементов');
        }

        // Image download
        $imageUrl = $this->getImageUri($article);

        if ($imageUrl !== null) {
            $imageFileName = $this->getFileNameByUrl($imageUrl);
            $this->downloadImage($imageUrl, $imageFileName);
        }

        return compact('title', 'content', 'imageFileName');
    }

    /**
     * @return Iterator
     */
    public function fetch(): Iterator
    {
        $result = new SplQueue();

        foreach ($this->getNewsLinks() as $link) {
            try {
                $result->push($this->getArticleByUrl($link));
            } catch (Exception $exception) {
                continue;
            }
        }

        return $result;
    }

    /**
     * @param string $url
     * @return Parser
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param int $limit
     * @return Parser
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param string $pathToSavedImages
     * @return Parser
     */
    public function setPathToSavedImages(string $pathToSavedImages): self
    {
        $this->pathToSavedImages = $pathToSavedImages;

        return $this;
    }

    /**
     * @param string $newsContainerClass
     * @return Parser
     */
    public function setNewsContainerClass(string $newsContainerClass): self
    {
        $this->newsContainerClass = $newsContainerClass;

        return $this;
    }

    /**
     * @param string $newsItemClass
     * @return Parser
     */
    public function setNewsItemClass(string $newsItemClass): self
    {
        $this->newsItemClass = $newsItemClass;

        return $this;
    }

    /**
     * @param string $newsContentClass
     * @return $this
     */
    public function setNewsContentClass(string $newsContentClass): self
    {
        $this->newsContentClass = $newsContentClass;

        return $this;
    }

    /**
     * @param string $articleImageClass
     * @return $this
     */
    public function setArticleImageClass(string $articleImageClass): self
    {
        $this->articleImageClass = $articleImageClass;

        return $this;
    }
}
