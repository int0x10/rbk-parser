<?php

namespace Int0x10\RbkParser;

use Exception;
use Iterator;

/**
 *
 */
class ParserBuilder
{
    private array $config;

    public function __construct()
    {
        $this->config = require dirname(__DIR__) . '/config/parser.php';
    }

    /**
     * @throws Exception
     */
    public function get(string $type): Iterator
    {
        if (isset($this->config[$type]) === false) {
            throw new Exception('Unknown parser type');
        }

        return (new Parser())
            ->setUrl($this->config[$type]['url'])
            ->setLimit($this->config['limit'])
            ->setPathToSavedImages($this->config['path_to_saved_images'])
            ->setNewsContainerClass($this->config[$type]['news_container_class'])
            ->setNewsItemClass($this->config[$type]['news_item_class'])
            ->setNewsContentClass($this->config[$type]['news_content_class'])
            ->setArticleImageClass($this->config[$type]['image_item_class'])
            ->fetch();
    }
}
