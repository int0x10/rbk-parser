<?php

use Doctrine\ORM\EntityManagerInterface;
use Int0x10\RbkParser\AppContainer;
use Int0x10\RbkParser\Entity\Article;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Twig\Error\LoaderError;

require dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Setup
 */
AppFactory::setContainer(new AppContainer());
$app = AppFactory::create();
$parserConfig = require dirname(__DIR__) . '/config/parser.php';

try {
    $app->add(TwigMiddleware::create(
        $app,
        Twig::create(dirname(__DIR__) . '/templates', [
            'cache' => false
        ])
    ));
} catch (LoaderError $e) {
    die($e->getMessage());
}

/**
 * Routes
 */
$app->get('/', function (Request $request, Response $response) use ($parserConfig): Response {
    /** @var EntityManagerInterface $em */
    $em = $this->get('doctrine');
    $view = Twig::fromRequest($request);

    return $view->render($response, 'index.html.twig', [
        'articles' => $em->getRepository(Article::class)->findBy([], [
            'id' => 'DESC',
        ], $parserConfig['limit'])
    ]);
});

$app->get('/article/{id}', function (Request $request, Response $response, array $args): Response {
    /** @var EntityManagerInterface $em */
    $em = $this->get('doctrine');

    /** @var Article | null $article */
    $article = $em->getRepository(Article::class)->find($args['id']);

    if ($article === null) {
        return $response->withStatus(404, 'Article Not Found');
    }

    $view = Twig::fromRequest($request);

    return $view->render($response, 'article.html.twig', [
        'article' => $article,
    ]);
});

$app->get('/images/{image}', function (Request $request, Response $response, array $args) use ($parserConfig): Response {
    $filePath = $parserConfig['path_to_saved_images'] . '/' . $args['image'];

    if (file_exists($filePath) === false) {
        return $response->withStatus(404, 'File Not Found');
    }

    $response = $response
        ->withHeader('Content-Type', mime_content_type($filePath));

    $response
        ->getBody()
        ->write(file_get_contents($filePath));

    return $response;
});

$app->run();
